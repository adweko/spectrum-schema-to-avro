# SpectrumSchemaToAvroSchemaConverter

## Beschreibung
Java-tooling um aus Pitney Bowes Spectrum exportierte WADL-Spezifikationen von Webschnittstellen automatisch 
in die Avro-Schemata der entsprechend ausgegebenen JSON-Nachrichten zu übersetzen.
Die wird benötigt, wenn im Anschluss von JSON nach Avro konvertiert wird, um z.B. die Antwort effizient in ein 
Kafka-Topic weiterzuleiten.

Derzeit gibt es zwei Use-Cases im MDM.

####1) Befüllen des Topics ch-mdm.state.all
CVBoost sendet die Antwort des Spectrum-Services *readFullPartnerAllVersionsFromDate* ohne weitere Modifikation
nach Kafka weiter. Entsprechend kann das korrespondierende Schema direkt aus der WADL
generiert werden.

####2) Befüllen des Topics ch-mdm.change.all
Hier ruft CVBoost mehrere Subflows im Spectrum auf und fügt deren Antworten zu einem
homogenisierten Change-Schema zusammen.
Die aufgerufenen Subflows sind *inboundTrafo(ZEPAS/Syrius)(Channel/Location/NaturalPerson/UnderWriting/LegalPerson)*.
Deren Ergebnisse werden in ein übergreifendes Oberschema gepackt, mit entsprechenden Einträgen die
dann eine oder mehrere Responses dieser Flows enthalten.

Ein Beispiel für LegalPerson ist weiter unten gezeigt.

## Benutzung
Die Ausführung der Main-Klasse generiert aus den WADL-Schemas, die unter 
*src/main/resources/schemata/wadl* abgelegt sind, die entsprechenden Avro-Schemas nach
*src/main/resources/schemata/avro*.

## Neue Schemas
Wird nur die Definition der Webschnittstellen im Spectrum angepasst, so reicht es die WADL-Dateien zu ersetzen
und Main zu starten, um die Schemas zu generieren. Testfälle müssen natürlich angepasst werden.
Für alle anderen möglichen Änderungen ist es nötig, den Code anzupassen.
Unter src/main/scripts/spectrum/fetch_wadls.sh
findet sich ein Skript, das automatisch die neueste Version der Webservice-Schemas exportiert und an die richtige Stelle im Projekt legt.

Danach fortfahren mit der Registrierung der Schemata in der Confluent Schema Registry. Siehe README unter src/main/scripts/schemareg.

#### Change-Beispiel für LegalPerson
```json
{
  "BusinessPartnerID": [
    {
      // present when LegalPerson changed
      "TECHInternalPID_LegalPerson": "33597FAA-86BF-4660-8A3F-B1E2FCF69525",
      "hasUpdate": true,
      "user_fields": [],
      "TECHMDMPartnerID": "C67F5121-E03F-492C-9971-4CB2B7695DF4",
      "TECHUser": "CH00BGR",
      "isNaturalPerson": [],
      "isLegalPerson": [
        {
          // zero or more entries
          // entries present when LegalPerson changed
        }
      ],
      "isBusinessPartner": [
        {
          // one or more entries        
        }
      ],
      "TECHExternalPID": "76daed1c-6f9b-42aa-9aed-1c6f9b12aa72",
      "TECHSourceSystem": "CH_ZEPAS",
      "hasIdentificationNumber": [
        {
          // one or more entries
        }
      ],
      "TECHInternalPID_Channel": "9DD0E858-2C2B-4F23-AF58-8FE310FAA954",
      "hasChannelID": [
        {
          // zero or more entries        
        }
      ],
      "TECHInternalPID_Location": "8E1F08B3-0896-4EC1-871B-EF909635B4A0"
    }
  ]
}
```


### WADL nach Avro-Konvertierung -- Besonderheiten  

Das WADL-File wird als XML eingelesen. 
Aus dem Eintrag ```<grammars>``` des WADL-Files wird der dritte Eintrag extrahiert, dieser entrpricht dem XSD-Schema
der Antwort des Webservice.
Dieses Schema wird über die Bibliothek xml-avro von XSD nach Avro konvertiert.
Bei dieser Konvertierung sind zwei Besonderheiten der WADLs zu beachten, die programmatisch korrigiert werden.
1) Das Schema enthält folgendes Muster:
```json
{
  "name" : "Versions",
  "type" : [ "null", {
    "type" : "record",
    "name" : "AnonType_Versionsjson_Response_Row",
    "fields" : [ {
      "name" : "Versions",
      "type" : [ "null", {
        "type" : "array",
        "items" : "<...>"
      } ],
    "default" : null,
    "source" : "..."
    } ]
  } ],
  "default" : null,
  "source" : "...."
}
```
    Berichtigt muss es so aussehen:
```json
{
"name" : "Versions",
"type" : [
  "null", 
  {
    "type" : "array",
    "items" : "<...>"
  }
],
"default" : null,
"source" : "..."
}
```
2) Alle Felder vom Typ xs:date haben ein komplexes, Spectrum-spezifisches Schema
    ```json
      {
                        "type" : "record",
                        "name" : "dateFromXsDate",
                        "fields" : [ {
                          "name" : "day",
                          "type" : "int"
                        }, {
                          "name" : "month",
                          "type" : "int"
                        }, {
                          "name" : "year",
                          "type" : "int"
                        } ]
                      }
    ```
    Daher werden alle Felder mit Typ xs:date zunächst auf ein Avro-int mit logicalType "date"
    konvertiert und diese danach in einem Korrekturlauf durch den komplexen date-Typ ersetzt.
 
## Development notes
- uses https://github.com/GeethanadhP/xml-avro for conversion
    - compiling that requires Java 8 (9+ does not work, some lib is missing)
    - not available in the maven repos, have to install locally
    - compiling with `gradlew build` complains that `https` is needed. just put the `s` into `https` in the url of the maven repositories in `build.gradle`
    - compile locally and point systemPath in pom.xml to your installation (Note: could do better)
    - put the jar it into libs here, called *xml-avro-all-1.8.2.jar*
- Exported WADL files are actually .xsd, not .xml as their name suggests
- The pom.xml generates classes from the Avro schemata. This does not make sense if Main() did not run before.
    Be aware of that. Typically, you would remove the generated classes, run Main.main() from your IDE (without maven)
    and then mvn compile to get the schemata.
- with the Avro schemata compiled to classes, had to 
    ```bash git config core.longpaths true```
    to check them in (on Windows 10 at least)

## Possible Bug
"Premature end-of-file error" when downloading and directly converting new schemas.
Auto-formatting the xml in IntelliJ solves that.
Need to check if IntelliJ introduces some strange line breaks or so to the xml.
Bug confirmed. Need to open each new xml and presst Ctrl+Alt+L. Then it works