package com.helvetia.app.mdm.spectrum;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.PrettyPrinter;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Equivalence;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.junit.jupiter.api.*;
import org.xml.sax.SAXException;
import tech.allegro.schema.json2avro.converter.JsonAvroConverter;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.jupiter.api.Assertions.fail;

public class convertExampleMessagesTest {

    private static Schema fullSchema;
    private static Schema changeSchema;

    private static String currentTestName;

    private final JsonAvroConverter converter = new JsonAvroConverter();

    private Path convert(Path wadl, Path avsc, String nameSpacePrefix) {
        try {
            ConvertSpectrumWADLToAvroSchema.main(new String[]{"--wadl", wadl.toAbsolutePath().toString(),
                    "--avsc", avsc.toAbsolutePath().toString(),
                    "--prefix", nameSpacePrefix});
        } catch (ParserConfigurationException | SAXException | IOException e) {
            fail(e);
        }
        return avsc;
    }

    @BeforeAll
    public static void runMain() throws IOException {
        Main.main(new String[]{});
        fullSchema = new Schema.Parser().parse(Paths.get("src/main/resources/schemata/avro/readFullPartnerAllVersionsFromDate.avsc").toFile());
        changeSchema = new Schema.Parser().parse(Paths.get("src/main/resources/schemata/avro/changes.avsc").toFile());
    }


    @BeforeEach
    public void prepare(TestInfo testInfo) {
        currentTestName = testInfo.getDisplayName();
    }

    @Test
    public void convertInitialState() throws IOException {
        byte[] msg = Files.readAllBytes(Paths.get("src/main/resources/exampleMessages/fullPartner/initial_create_state.json"));
        GenericData.Record record = converter.convertToGenericDataRecord(msg, fullSchema);
        Assertions.assertTrue(compareJsons(msg, record.toString()));
    }

    @Test
    public void convertDubletteState() throws IOException {
        byte[] msg = Files.readAllBytes(Paths.get("src/main/resources/exampleMessages/fullPartner/dublette_state.json"));
        GenericData.Record record = converter.convertToGenericDataRecord(msg, fullSchema);
        Assertions.assertTrue(compareJsons(msg, record.toString()));
    }

    @Test
    public void convertUnderwritingState() throws IOException {
        byte[] msg = Files.readAllBytes(Paths.get("src/main/resources/exampleMessages/fullPartner/uw_state.json"));
        GenericData.Record record = converter.convertToGenericDataRecord(msg, fullSchema);
        Assertions.assertTrue(compareJsons(msg, record.toString()));
    }

    @Test
    public void convertTechUserState() throws IOException {
        byte[] msg = Files.readAllBytes(Paths.get("src/main/resources/exampleMessages/fullPartner/techuser_new_field_state.json"));
        GenericData.Record record = converter.convertToGenericDataRecord(msg, fullSchema);
        Assertions.assertTrue(compareJsons(msg, record.toString()));
    }

    @Test
    public void convertInitialChange() throws IOException {
        byte[] msg = Files.readAllBytes(Paths.get("src/main/resources/exampleMessages/changePartner/inital_create_change.json"));
        GenericData.Record record = converter.convertToGenericDataRecord(msg, changeSchema);
        // Is OK, checked by hand, Email Problem explained inc compareJsons()
        Assertions.assertTrue(compareJsons(msg, record.toString()));
    }

    @Test
    public void convertDubletteChange() throws IOException {
        byte[] msg = Files.readAllBytes(Paths.get("src/main/resources/exampleMessages/changePartner/dublette_change.json"));
        GenericData.Record record = converter.convertToGenericDataRecord(msg, changeSchema);
        // Is OK, checked by hand, Email Problem explained inc compareJsons()
        Assertions.assertTrue(compareJsons(msg, record.toString()));
    }

    @Test
    public void convertUnderwritingChange() throws IOException {
        byte[] msg = Files.readAllBytes(Paths.get("src/main/resources/exampleMessages/changePartner/uw_change.json"));
        GenericData.Record record = converter.convertToGenericDataRecord(msg, changeSchema);
        Assertions.assertTrue(compareJsons(msg, record.toString()));
    }

    @Test
    public void convertTechUserChange() throws IOException {
        byte[] msg = Files.readAllBytes(Paths.get("src/main/resources/exampleMessages/changePartner/techuser_new_field_change.json"));
        GenericData.Record record = converter.convertToGenericDataRecord(msg, changeSchema);
        Assertions.assertTrue(compareJsons(msg, record.toString()));
    }


    private boolean compareJsons(byte[] referenceJson, String customJson) throws IOException {

        // TODO: make json compare ignore empty subelements completely
        // We still have differences of the type
        // "function": ""
        // and of the type
        // "hasEMail": [
        //                {
        //                  "EMail": [
        //                    {}
        //                  ]
        //                }
        //              ],
        // which are fine, but painful to ignore in comparison
//            Assertions.assertTrue(compareJsons(msg, record.toString()));

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_ABSENT);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
        mapper.configure(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY, true);
        mapper.configure(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS, true);
        TypeReference<HashMap<String, Object>> type =
                new TypeReference<HashMap<String, Object>>() {
                };

        HashMap<String, Object> reference = null;
        HashMap<String, Object> custom = null;
        try {
            reference = mapper.readValue(referenceJson, type);
            custom = mapper.readValue(customJson, type);
        } catch (IOException e) {
            fail(e);
        }
        // since one json contains a lot of null entries, we write the jsons out
        // without any nulls
        String sortedReferenceJson;
        String sortedCustomJson;

        HashMap<String, Object> cleanedReference;
        HashMap<String, Object> cleanedCustom;
        try {
            sortedReferenceJson = mapper.writeValueAsString(reference);
            sortedCustomJson = mapper.writeValueAsString(custom);
            // all nulls should be gone now
            // so read them in again to map, for comparison
            cleanedReference = mapper.readValue(sortedReferenceJson, type);
            cleanedCustom = mapper.readValue(sortedCustomJson, type);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        MapDifference<String, Object> difference = Maps.difference(cleanedReference, cleanedCustom);
        if (difference.areEqual()) {
            return true;
        } else {
            try {
                System.out.println("reference: " + mapper.writeValueAsString(reference));
                System.out.println("custom: " + mapper.writeValueAsString(custom));
                System.out.println("differences: " + difference.entriesDiffering());
                System.out.println("Please check the generated files in src/test/resources/tmp and see if they work for you!");
                mapper.writerWithDefaultPrettyPrinter().writeValue(
                        Paths.get("src/test/resources/tmp/" + currentTestName + "_reference.json").toFile(),
                        reference
                );
                mapper.writerWithDefaultPrettyPrinter().writeValue(
                        Paths.get("src/test/resources/tmp/" + currentTestName + "_converted.json").toFile(),
                        custom
                );
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            return false;
        }

    }


}
