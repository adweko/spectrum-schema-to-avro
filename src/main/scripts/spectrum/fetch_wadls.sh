# fetch the current wadls from Spectrum and put them into the correct folder

export ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd | awk -F 'src' '{print $1}')"
echo "ROOTDIR is $ROOTDIR"

SPECTRUM_ADDRESS="https://mdm-spectrum-intg.helvetia.ch:8443"

curl --insecure ${SPECTRUM_ADDRESS}/rest/readFullPartnerAllVersionsFromDate?_wadl --output readFullPartnerAllVersionsFromDate.xml
curl --insecure ${SPECTRUM_ADDRESS}/rest/inboundTrafoZepasChannel?_wadl --output inboundTrafoZepasChannel.xml
curl --insecure ${SPECTRUM_ADDRESS}/rest/inboundTrafoZepasLegalPerson?_wadl --output inboundTrafoZepasLegalPerson.xml
curl --insecure ${SPECTRUM_ADDRESS}/rest/inboundTrafoZepasLocation?_wadl --output inboundTrafoZepasLocation.xml
curl --insecure ${SPECTRUM_ADDRESS}/rest/inboundTrafoZepasNaturalPerson?_wadl --output inboundTrafoZepasNaturalPerson.xml
curl --insecure ${SPECTRUM_ADDRESS}/rest/inboundTrafoZepasUnderwriting?_wadl --output inboundTrafoZepasUnderwriting.xml
curl --insecure ${SPECTRUM_ADDRESS}/rest/inboundTrafoZepasDuplicates?_wadl --output inboundTrafoZepasDuplicates.xml

mv *.xml ${ROOTDIR}/src/main/resources/schemata/wadl/