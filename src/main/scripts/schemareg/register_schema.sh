# path to project root
export ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd | awk -F 'src' '{print $1}')"

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file> -f <schema file> -s <schema subject>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:f:s:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    f) # Specify schema file.
      export SCHEMA_FILE=${OPTARG}
      ;;
    s) # Specify schema subject
      export SCHEMA_SUBJECT=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}
[[ -z ${SCHEMA_FILE} ]] && usage "schema file not set"
[[ -z ${SCHEMA_SUBJECT} ]] && usage "schema subject not set"


# schema registry wants the request wrapped as json { "schema" : " { <avsc where all quotes are escaped> } "}
# we first write this to a temporary file and then send that to the schema registry
tmpfile=${ROOTDIR}/tmp.avsc
echo '{"schema" : '\""$(sed 's@\"@\\\"@g' "${SCHEMA_FILE}" )"\"'}' > "${tmpfile}"
# actual request
curl -k -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
  --data @"${tmpfile}" \
 ${SCHEMAREG_URL}/subjects/${SCHEMA_SUBJECT}/versions



