# path to project root
export ROOTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd | awk -F 'src' '{print $1}')"
# schema registry wants the request wrapped as json { "schema" : " { <avsc where all quotes are escaped> } "}
# we first write this to a temporary file and then send that to the schema registry
tmpfile=${ROOTDIR}/tmp.avsc
echo '{"schema" : '\""$(sed 's@\"@\\\"@g' "${ROOTDIR}/src/main/resources/schemata/avro/readFullPartnerAllVersionsFromDate.avsc" )"\"'}' > "${tmpfile}"
# actual request
curl -k -X POST -H "Content-Type: application/vnd.schemaregistry.v1+json" \
  --data @"${tmpfile}" \
  https://cp-schema-registry-kafka-devl.apps.helvetia.io/subjects/ch-mdm.state.all/versions



