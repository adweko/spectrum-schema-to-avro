export ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd | awk -F 'src' '{print $1}')"

usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "Get latest schema version for subject from schema reg."
    echo "usage: $0 -e <env-file>"
    exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:s:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    s) # Specify schema subject
      export SCHEMA_SUBJECT=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}
[[ -z ${SCHEMA_SUBJECT} ]] && usage "schema subject not set"

curl -k -X GET ${SCHEMAREG_URL}/subjects/${SCHEMA_SUBJECT}/versions/latest