# Schema Registry Integration

## Kurzzusammenfassung

Skripte unter src/main/scripts/schemareg benutzen, um Schemas in der Confluent Schema Registry (CSR)
zu registrieren. Doku an den Skripten, wenn alles Standard ist nur:

```bash
bash do_default_work.sh -e env/<ENV-YOU-WANT>
```

Die folgenden Schemas werden registriert:

| Entity             | Topic               | Subject in CSR     | Compatibility |  
| ------------------ | ------------------- | -----------------  | ------------- |
| full Partner       | ch-mdm.state.all    | ch-mdm.state.all   | FORWARD       |
| changes on Partner | ch-mdm.change.all   | ch-mdm.change.all  | FORWARD       |

"full Partner" entspricht *src/main/resources/schemata/avro/readFullPartnerAllVersionsFromDate.avsc*,
"changes on Partner" entspricht *src/main/resources/schemata/avro/changes.avsc*.


## Kontext
Die Helvetia Container Plattform (HCP), auf der auch Kafka läuft, benutzt die Confluent Schema Registry
(kurz: CSR) zur Verwaltung der Avro-Schemas.
Die CSR legt Schemas unter einem "Subject" ab. Dieses definiert einen Namensraum in der CSR, in dem
dann die Schemas zu einer bestimmten Entität zu finden sind.
Pro Subject (Entität) können konsekutiv mehrere Schemas registriert werden, diese werden dann mit
aufsteigender Versionsnummer abgelegt.
Die CSR bietet die Möglichkeit, bei Folge-Registrierungen (Schema Evolution) die Schemakompatibilität
zu einer oder allen vorhergehenden Versionen zu überprüfen und sicherzustellen.

## Einsatz im MDM
Die MDM-Prozesse schreiben zwei Entitäten nach Kafka:
1) Vollversionen der Partnerdaten in das Topic "ch-mdm.state.all"
2) Änderungen von Partnern in das Topic "ch-mdm.change.all"

Die relevanten Schemas für die Topics liegen unter src/main/resources/schemata/avro und heißen
readFullPartnerAllVersionsFromDate.avsc für das Topic "ch-mdm.state.all"
changes.avsc für das Topic "ch-mdm.change.all".

Diese Schemas müssen in der CSR registriert werden.
Pro Topic wird nur eine Entität geschrieben, so dass sich eine Benamungsstrategie (Subejct Naming Strategy)
analog der "TopicNameStrategy" des 
[KafkaAvroSerializers](https://docs.confluent.io/current/schema-registry/serdes-develop/index.html)
anbietet: Als Subjektname wird der Topic-Name verwendet.

### Schema Evolution - Kompatibilitätslevel
Das Datenmodell des MDM ist absehbar nur selten Änderungen unterworfen. Aufgrund der Besonderheiten des MDM-Modells
und der Definition der Webservice-Antworten von Spectrum sind die meisten Felder der MDM-Nachrichten optional.
Nicht-optionale Felder in der Schema-Defintion sind nur durch applikationsseitige Garantien möglich und müssen
manuell ins Schema eingebaut werden (Stand 2020/05/20: keine vorhanden).

Durch diese Besonderheiten sind alle Änderungen im Schema, die automatisch eingearbeitet werden können, immer
kompatibel, da es sich ausschließlich um optionale Felder handelt.
Ein Nutzen für die Konsumenten ist damit erst gegeben, wenn applikationsseitige Garantien eingearbeitet werden.
Daher empfiehlt sich, für die CSR das Kompatibilitätslevel 
["FORWARD"](https://docs.confluent.io/current/schema-registry/avro.html)  
zu setzen. Dieses sorgt dafür, dass das Hinzufügen nicht-optionaler Felder erlaubt ist
(hinzukommen applikationsseitiger Garantien, erwünscht).
Die MDM-Logik kann kompatibel geupdatet werden, ohne dass die Konsumenten zwangsweise
angepasst werden müssen (auf technischer Ebene). 
Der Wegfall von Garantien wird erschwert, derartige Änderungen werden abgelehnt und erfordern einen umfassenderen
Migrationsprozess auf ein neues Topic.

 
 