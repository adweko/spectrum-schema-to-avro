usage() {
    [[ $1 ]] && echo "Error: $1"
    echo "usage: $0 -e <env-file> -s <schema subject>"
    exit 0
}
[[ $# -eq 0 ]] && usage


while getopts ":he:f:s:" arg; do
  case $arg in
    e) # Specify env file.
      export ENV_FILE=${OPTARG}
      ;;
    s) # Specify schema subject
      export SCHEMA_SUBJECT=${OPTARG}
      ;;
    h | *) # Display usage and exit
      usage
      ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}
[[ -z ${SCHEMA_SUBJECT} ]] && usage "schema subject not set"

curl -k -X PUT -H "Content-Type: application/vnd.schemaregistry.v1+json" \
  --data '{"compatibility": "FORWARD"}' \
  ${SCHEMAREG_URL}/config/${SCHEMA_SUBJECT}



