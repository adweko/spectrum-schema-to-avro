# path to project root
export ROOTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd | awk -F 'src' '{print $1}')"

usage() {
  [[ $1 ]] && echo "Error: $1"
  echo "Default case. Registers change and state schema, sets compatibility to forward and prints the new version numbers."
  echo "usage: $0 -e <env-file>"
  exit 0
}
[[ $# -eq 0 ]] && usage

while getopts ":he:f:s:" arg; do
  case $arg in
  e) # Specify env file.
    export ENV_FILE=${OPTARG}
    ;;
  h | *) # Display usage and exit
    usage
    ;;
  esac
done

[[ -z ${ENV_FILE} ]] && usage "env file not set"
source ${ENV_FILE}

bash "$ROOTDIR"/src/main/scripts/schemareg/register_schema.sh -e "$ENV_FILE" -f $ROOTDIR/src/main/resources/schemata/avro/changes.avsc -s ch-mdm.change.all
bash "$ROOTDIR"/src/main/scripts/schemareg/register_schema.sh -e "$ENV_FILE" -f $ROOTDIR/src/main/resources/schemata/avro/readFullPartnerAllVersionsFromDate.avsc -s ch-mdm.state.all

bash "$ROOTDIR"/src/main/scripts/schemareg/set_compatibility_to_forward.sh -e "$ENV_FILE" -s ch-mdm.change.all
bash "$ROOTDIR"/src/main/scripts/schemareg/set_compatibility_to_forward.sh -e "$ENV_FILE" -s ch-mdm.state.all

change_version=$(bash "$ROOTDIR"/src/main/scripts/schemareg/helpers/get_latest_schema_for_subject.sh -e "$ENV_FILE" -s ch-mdm.change.all | awk -F ',"id":' '{print $1}' | awk -F '"version":' '{print $2}')

echo "newest version of change schema is $change_version on $SCHEMAREG_URL"

state_version=$(bash "$ROOTDIR"/src/main/scripts/schemareg/helpers/get_latest_schema_for_subject.sh -e "$ENV_FILE" -s ch-mdm.state.all | awk -F ',"id":' '{print $1}' | awk -F '"version":' '{print $2}')

echo "newest version of state schema is $state_version on $SCHEMAREG_URL"