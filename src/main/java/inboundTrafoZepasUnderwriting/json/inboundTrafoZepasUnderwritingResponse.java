/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package inboundTrafoZepasUnderwriting.json;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class inboundTrafoZepasUnderwritingResponse extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 8939483015748713791L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"inboundTrafoZepasUnderwritingResponse\",\"namespace\":\"inboundTrafoZepasUnderwriting.json\",\"fields\":[{\"name\":\"BusinessPartnerID\",\"type\":{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"Row\",\"namespace\":\"inboundTrafoZepasUnderwriting.json.Response\",\"fields\":[{\"name\":\"Status\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:Status\"},{\"name\":\"Status_Code\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:Status.Code\"},{\"name\":\"Status_Description\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:Status.Description\"},{\"name\":\"hasUpdate\",\"type\":[\"null\",\"boolean\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:hasUpdate\"},{\"name\":\"recordID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:recordID\"},{\"name\":\"TECHMDMPartnerID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHMDMPartnerID\"},{\"name\":\"CRUD\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:CRUD\"},{\"name\":\"isBusinessPartner\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"isBusinessPartnerEntry\",\"namespace\":\"inboundTrafoZepasUnderwriting.json.Response.RowSchema.isBusinessPartner\",\"fields\":[{\"name\":\"BusinessPartner\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"BusinessPartnerEntry\",\"namespace\":\"inboundTrafoZepasUnderwriting.json.Response.RowSchema.isBusinessPartner.isBusinessPartnerSchema.BusinessPartner\",\"fields\":[{\"name\":\"encryptionCode_VIP\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:encryptionCode_VIP\"},{\"name\":\"newBusinessPartnerID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:newBusinessPartnerID\"},{\"name\":\"TECHExternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHExternalPID\"},{\"name\":\"TECHInternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHInternalPID\"},{\"name\":\"TECHMDMPartnerID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHMDMPartnerID\"},{\"name\":\"TECHSourceSystem\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHSourceSystem\"},{\"name\":\"_stp_label\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:_stp_label\"},{\"name\":\"_stp_id\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:_stp_id\"},{\"name\":\"_stp_type\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:_stp_type\"}]}}],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BusinessPartner\"},{\"name\":\"BITEMPDeleted\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPDeleted\"},{\"name\":\"BITEMPMDMTimestamp\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPMDMTimestamp\"},{\"name\":\"BITEMPSourceTimestamp\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPSourceTimestamp\"},{\"name\":\"BITEMPValidFrom\",\"type\":[\"null\",{\"type\":\"record\",\"name\":\"dateFromXsDate\",\"namespace\":\"inboundTrafoZepasUnderwriting.com.helvetia.custom\",\"fields\":[{\"name\":\"day\",\"type\":\"int\"},{\"name\":\"month\",\"type\":\"int\"},{\"name\":\"year\",\"type\":\"int\"}]}],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPValidFrom\"},{\"name\":\"BITEMPValidTo\",\"type\":[\"null\",\"inboundTrafoZepasUnderwriting.com.helvetia.custom.dateFromXsDate\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPValidTo\"},{\"name\":\"TECHExternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHExternalPID\"},{\"name\":\"TECHInternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHInternalPID\"},{\"name\":\"TECHMDMPartnerID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHMDMPartnerID\"},{\"name\":\"TECHSourceSystem\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHSourceSystem\"},{\"name\":\"_stp_id\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:_stp_id\"},{\"name\":\"_stp_key\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:_stp_key\"},{\"name\":\"_stp_label\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:_stp_label\"}]}}],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:isBusinessPartner\"},{\"name\":\"BusinessPartnerID_TECHExternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BusinessPartnerID.TECHExternalPID\"},{\"name\":\"BusinessPartnerID_TECHInternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BusinessPartnerID.TECHInternalPID\"},{\"name\":\"BusinessPartnerID_TECHSourceSystem\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BusinessPartnerID.TECHSourceSystem\"},{\"name\":\"hasUnderwritingCode\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"hasUnderwritingCodeEntry\",\"namespace\":\"inboundTrafoZepasUnderwriting.json.Response.RowSchema.hasUnderwritingCode\",\"fields\":[{\"name\":\"UnderwritingCode\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"UnderwritingCodeEntry\",\"namespace\":\"inboundTrafoZepasUnderwriting.json.Response.RowSchema.hasUnderwritingCode.hasUnderwritingCodeSchema.UnderwritingCode\",\"fields\":[{\"name\":\"stopCode\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:stopCode\"},{\"name\":\"TECHExternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHExternalPID\"},{\"name\":\"TECHInternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHInternalPID\"},{\"name\":\"TECHSourceSystem\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHSourceSystem\"},{\"name\":\"TECHUser\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHUser\"},{\"name\":\"hasIdentificationNumber\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"hasIdentificationNumberEntry\",\"namespace\":\"inboundTrafoZepasUnderwriting.json.Response.RowSchema.hasUnderwritingCode.hasUnderwritingCodeSchema.UnderwritingCode.UnderwritingCodeSchema.hasIdentificationNumber\",\"fields\":[{\"name\":\"IdentificationNumber\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"IdentificationNumberEntry\",\"namespace\":\"inboundT","rafoZepasUnderwriting.json.Response.RowSchema.hasUnderwritingCode.hasUnderwritingCodeSchema.UnderwritingCode.UnderwritingCodeSchema.hasIdentificationNumber.hasIdentificationNumberSchema.IdentificationNumber\",\"fields\":[{\"name\":\"IDNumber\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:IDNumber\"},{\"name\":\"IDType\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:IDType\"},{\"name\":\"TECHExternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHExternalPID\"},{\"name\":\"TECHInternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHInternalPID\"},{\"name\":\"TECHMDMPartnerID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHMDMPartnerID\"},{\"name\":\"TECHSourceSystem\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHSourceSystem\"},{\"name\":\"TECHUser\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHUser\"}]}}],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:IdentificationNumber\"},{\"name\":\"BITEMPDeleted\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPDeleted\"},{\"name\":\"BITEMPMDMTimestamp\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPMDMTimestamp\"},{\"name\":\"BITEMPSourceTimestamp\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPSourceTimestamp\"},{\"name\":\"BITEMPValidFrom\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPValidFrom\"},{\"name\":\"BITEMPValidTo\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPValidTo\"},{\"name\":\"TECHExternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHExternalPID\"},{\"name\":\"TECHInternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHInternalPID\"},{\"name\":\"TECHMDMPartnerID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHMDMPartnerID\"},{\"name\":\"TECHSourceSystem\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHSourceSystem\"},{\"name\":\"TECHUser\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHUser\"}]}}],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:hasIdentificationNumber\"}]}}],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:UnderwritingCode\"},{\"name\":\"BITEMPDeleted\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPDeleted\"},{\"name\":\"BITEMPMDMTimestamp\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPMDMTimestamp\"},{\"name\":\"BITEMPSourceTimestamp\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPSourceTimestamp\"},{\"name\":\"TECHExternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHExternalPID\"},{\"name\":\"TECHInternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHInternalPID\"},{\"name\":\"TECHMDMPartnerID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHMDMPartnerID\"},{\"name\":\"TECHSourceSystem\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHSourceSystem\"},{\"name\":\"TECHUser\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:TECHUser\"},{\"name\":\"BITEMPValidFrom\",\"type\":[\"null\",\"inboundTrafoZepasUnderwriting.com.helvetia.custom.dateFromXsDate\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPValidFrom\"},{\"name\":\"BITEMPValidTo\",\"type\":[\"null\",\"inboundTrafoZepasUnderwriting.com.helvetia.custom.dateFromXsDate\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:BITEMPValidTo\"}]}}],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:hasUnderwritingCode\"},{\"name\":\"user_fields\",\"type\":[\"null\",{\"type\":\"array\",\"items\":{\"type\":\"record\",\"name\":\"field\",\"namespace\":\"inboundTrafoZepasUnderwriting.user\",\"fields\":[{\"name\":\"name\",\"type\":\"string\",\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:name\"},{\"name\":\"value\",\"type\":\"string\",\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:value\"}]}}],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasUnderwriting:user_fields\"}]}}}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<inboundTrafoZepasUnderwritingResponse> ENCODER =
      new BinaryMessageEncoder<inboundTrafoZepasUnderwritingResponse>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<inboundTrafoZepasUnderwritingResponse> DECODER =
      new BinaryMessageDecoder<inboundTrafoZepasUnderwritingResponse>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<inboundTrafoZepasUnderwritingResponse> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<inboundTrafoZepasUnderwritingResponse> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<inboundTrafoZepasUnderwritingResponse>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this inboundTrafoZepasUnderwritingResponse to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a inboundTrafoZepasUnderwritingResponse from a ByteBuffer. */
  public static inboundTrafoZepasUnderwritingResponse fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.util.List<inboundTrafoZepasUnderwriting.json.Response.Row> BusinessPartnerID;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public inboundTrafoZepasUnderwritingResponse() {}

  /**
   * All-args constructor.
   * @param BusinessPartnerID The new value for BusinessPartnerID
   */
  public inboundTrafoZepasUnderwritingResponse(java.util.List<inboundTrafoZepasUnderwriting.json.Response.Row> BusinessPartnerID) {
    this.BusinessPartnerID = BusinessPartnerID;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return BusinessPartnerID;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: BusinessPartnerID = (java.util.List<inboundTrafoZepasUnderwriting.json.Response.Row>)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'BusinessPartnerID' field.
   * @return The value of the 'BusinessPartnerID' field.
   */
  public java.util.List<inboundTrafoZepasUnderwriting.json.Response.Row> getBusinessPartnerID() {
    return BusinessPartnerID;
  }

  /**
   * Sets the value of the 'BusinessPartnerID' field.
   * @param value the value to set.
   */
  public void setBusinessPartnerID(java.util.List<inboundTrafoZepasUnderwriting.json.Response.Row> value) {
    this.BusinessPartnerID = value;
  }

  /**
   * Creates a new inboundTrafoZepasUnderwritingResponse RecordBuilder.
   * @return A new inboundTrafoZepasUnderwritingResponse RecordBuilder
   */
  public static inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder newBuilder() {
    return new inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder();
  }

  /**
   * Creates a new inboundTrafoZepasUnderwritingResponse RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new inboundTrafoZepasUnderwritingResponse RecordBuilder
   */
  public static inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder newBuilder(inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder other) {
    return new inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder(other);
  }

  /**
   * Creates a new inboundTrafoZepasUnderwritingResponse RecordBuilder by copying an existing inboundTrafoZepasUnderwritingResponse instance.
   * @param other The existing instance to copy.
   * @return A new inboundTrafoZepasUnderwritingResponse RecordBuilder
   */
  public static inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder newBuilder(inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse other) {
    return new inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder(other);
  }

  /**
   * RecordBuilder for inboundTrafoZepasUnderwritingResponse instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<inboundTrafoZepasUnderwritingResponse>
    implements org.apache.avro.data.RecordBuilder<inboundTrafoZepasUnderwritingResponse> {

    private java.util.List<inboundTrafoZepasUnderwriting.json.Response.Row> BusinessPartnerID;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.BusinessPartnerID)) {
        this.BusinessPartnerID = data().deepCopy(fields()[0].schema(), other.BusinessPartnerID);
        fieldSetFlags()[0] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing inboundTrafoZepasUnderwritingResponse instance
     * @param other The existing instance to copy.
     */
    private Builder(inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.BusinessPartnerID)) {
        this.BusinessPartnerID = data().deepCopy(fields()[0].schema(), other.BusinessPartnerID);
        fieldSetFlags()[0] = true;
      }
    }

    /**
      * Gets the value of the 'BusinessPartnerID' field.
      * @return The value.
      */
    public java.util.List<inboundTrafoZepasUnderwriting.json.Response.Row> getBusinessPartnerID() {
      return BusinessPartnerID;
    }

    /**
      * Sets the value of the 'BusinessPartnerID' field.
      * @param value The value of 'BusinessPartnerID'.
      * @return This builder.
      */
    public inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder setBusinessPartnerID(java.util.List<inboundTrafoZepasUnderwriting.json.Response.Row> value) {
      validate(fields()[0], value);
      this.BusinessPartnerID = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'BusinessPartnerID' field has been set.
      * @return True if the 'BusinessPartnerID' field has been set, false otherwise.
      */
    public boolean hasBusinessPartnerID() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'BusinessPartnerID' field.
      * @return This builder.
      */
    public inboundTrafoZepasUnderwriting.json.inboundTrafoZepasUnderwritingResponse.Builder clearBusinessPartnerID() {
      BusinessPartnerID = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public inboundTrafoZepasUnderwritingResponse build() {
      try {
        inboundTrafoZepasUnderwritingResponse record = new inboundTrafoZepasUnderwritingResponse();
        record.BusinessPartnerID = fieldSetFlags()[0] ? this.BusinessPartnerID : (java.util.List<inboundTrafoZepasUnderwriting.json.Response.Row>) defaultValue(fields()[0]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<inboundTrafoZepasUnderwritingResponse>
    WRITER$ = (org.apache.avro.io.DatumWriter<inboundTrafoZepasUnderwritingResponse>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<inboundTrafoZepasUnderwritingResponse>
    READER$ = (org.apache.avro.io.DatumReader<inboundTrafoZepasUnderwritingResponse>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
