package com.helvetia.app.mdm.spectrum;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.commons.io.FilenameUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Converter {
    private final File wadlFile;
    private final File avscFile;
    // prefix for all namespaces in schema
    private final String nameSpacePrefix;

    public Converter(File wadlFile, File avscFile, String nameSpacePrefix) {
        this.wadlFile = wadlFile;
        this.avscFile = avscFile;
        this.nameSpacePrefix = nameSpacePrefix;
    }

    public void convert() throws IOException, SAXException, ParserConfigurationException {
        // The WADL we get describes the whole web interface
        // we are only interested in the part defining the schema of the "response"
        // In all the examples so far, this has been item 3 the definition of <grammars>
        // Note that this item(3) will have to be changed when the format changes
        final String responseSchemaXsd = getResponseSchemaFromXmlExport(wadlFile);
        // the raw XSD is converted to Avro using the xml-avro library
        // please be sure to not modify the config for that, especially the mapping from xs:date to Avro date
        final Schema rawSchema = convertXsdToAvro(responseSchemaXsd);
        // the schema of the web service response specifies more than just the data
        // hence, we extract only the data part, which is called json_<wadl-filename>
        final Schema relevantSchema = extractRelevantSubSchema(rawSchema);
        // the schema of interest we obtained has some deviations from what is observed in the messages
        // these are removed, and a corrected version is obtained
        final Schema correctedSchema = correctSchema(relevantSchema);
        // write corrected schema to file
        writeToOutputFile(correctedSchema);
    }

    private void writeToOutputFile(Schema correctedSchema) throws FileNotFoundException {
        try (PrintWriter out = new PrintWriter(avscFile)) {
            out.write(correctedSchema.toString(true));
        }
    }


    private Schema correctSchema(Schema relevantSchema) throws JsonProcessingException {
        // since the Avro Schema is too immutable, we parse it to json and modify that
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree(relevantSchema.toString());
        SchemaCorrector schemaCorrector = new SchemaCorrector();
        schemaCorrector.correct(jsonNode);
        return new Schema.Parser().parse(jsonNode.toString());
    }

    private class SchemaCorrector {

        private boolean dateFirstDefined = false;

        private void regenerate() {
            dateFirstDefined = false;
        }

        private ObjectNode getCorrectDateTypeInitial() {
            ObjectNode correctType = JsonNodeFactory.instance.objectNode();
            correctType.put("name", nameSpacePrefix.concat(".com.helvetia.custom.dateFromXsDate"));
            correctType.put("type", "record");
            ArrayNode dateFields = JsonNodeFactory.instance.arrayNode();
            ObjectNode monthField = JsonNodeFactory.instance.objectNode();
            monthField.put("name", "month");
            monthField.put("type", "int");
            ObjectNode dayField = JsonNodeFactory.instance.objectNode();
            dayField.put("name", "year");
            dayField.put("type", "int");
            ObjectNode yearField = JsonNodeFactory.instance.objectNode();
            yearField.put("name", "day");
            yearField.put("type", "int");
            dateFields.add(yearField);
            dateFields.add(monthField);
            dateFields.add(dayField);
            correctType.put("fields", dateFields);
            return correctType;
        }

        public void correct(JsonNode root) {
            if (root.isObject()) {
                ArrayList<String> fieldNames = new ArrayList<>();
                root.fieldNames().forEachRemaining(fieldNames::add);

                if (fieldNames.contains("type")) {
                    JsonNode type = root.get("type");
                    // if it is a nullable union with complex type, it may be problematic
                    if (type.isArray() && type.size() == 2 && type.get(0).asText().equals("null") && type.get(1).isObject()) {
                        JsonNode subType = type.get(1);
                        String typeOfSubType = subType.get("type").asText();
                        if (typeOfSubType.equals("record")) {
                            // fix the "Reponse_Row" problem
                            String subTypeName = subType.get("name").asText();
                            if (subTypeName.contains("Response_Row")) {
                                JsonNode correctType = subType.get("fields").get(0).get("type");
                                ((ObjectNode) root).replace("type", correctType);
                            }
                        } else if (typeOfSubType.equals("int")) {
                            // fix the date representation problem
                            // dates that are represented via xs:date in the original WADL
                            //
                            // However, the .json we get has this:
                            // "BITEMPValidTo": {
                            //				"month": 1,
                            //				"year": 3000,
                            //				"day": 1
                            //			},
                            // which has a schema
                            //        "name": "BITEMPValidTo",
                            //        "type": "record",
                            //        "fields": [
                            //          {
                            //            "name": "month",
                            //            "type": "int"
                            //          },
                            //          {
                            //            "name": "year",
                            //            "type": "int"
                            //          },
                            //          {
                            //            "name": "day",
                            //            "type": "int"
                            //          }
                            //        ]
                            //
                            // To identify this, we use the converter to map xs:date to {type "int", logicalType "date"}
                            // and convert this to the json representation

                            if (subType.has("logicalType") && subType.get("logicalType").asText().equals("date")) {
                                ArrayNode correctTypeNullable = JsonNodeFactory.instance.arrayNode();
                                correctTypeNullable.add("null");
                                if (!dateFirstDefined) {
                                    ObjectNode correctType = getCorrectDateTypeInitial();
//                                    correctType.set("namespace", JsonNodeFactory.instance.textNode(DATE_FROM_XS_DATE_NAMESPACE));
                                    this.dateFirstDefined = true;
                                    correctTypeNullable.add(correctType);
                                } else {
                                    String correctType = nameSpacePrefix.concat(".com.helvetia.custom.dateFromXsDate");// FilenameUtils.removeExtension(wadlFile.getName()) + ".dateFromXsDate";
                                    correctTypeNullable.add(correctType);
                                }

                                ((ObjectNode) root).replace("type", correctTypeNullable);
                            }

                        }    // else do nothing
                    } else if (type.asText().equals("record")) {
                        String name = root.get("name").asText();
                        List<String> parts = Arrays.asList(name.split(Pattern.quote("_")));
                        List<String> nameSpaceParts = parts.subList(0, parts.size() - 1);
                        // we need to correct the name when it is the same as the namespace
                        // this always happens for the type of items in arrays, which share the array's name
                        String correctedName = parts.get(parts.size() - 1);
                        if (parts.size() > 2 && correctedName.equals(parts.get(parts.size() - 2))) {
                            correctedName = correctedName + "Entry"; // E for Entry
                        }
                        // since namespaces are absolute and we do not drag them along in the recursion, we correct
                        // for this in the namespace as well
                        for (int i = 1; i < nameSpaceParts.size(); i++) {
                            if (nameSpaceParts.get(i).equals("Row")) {
                                nameSpaceParts.set(i, "RowSchema"); // ES for EntrySchema
                            }
                            if (nameSpaceParts.get(i - 1).equals(nameSpaceParts.get(i))) {
                                nameSpaceParts.set(i, nameSpaceParts.get(i) + "Schema");
                            }
                        }
                        String nameSpace = String.join(".", nameSpaceParts);
                        String newNameSpace;
                        if (!nameSpace.startsWith(nameSpacePrefix)) {
                            newNameSpace = nameSpacePrefix.concat(".").concat(nameSpace);
                        } else {
                            newNameSpace = nameSpace;
                        }

                        ((ObjectNode) root).replace("name", JsonNodeFactory.instance.textNode(correctedName));
                        ((ObjectNode) root).put("namespace", newNameSpace);
                    }
                }

                if (fieldNames.contains("namespace")) {
                    String nameSpace = root.get("namespace").asText();
                    if (!nameSpace.startsWith(nameSpacePrefix)) {
                        String newNameSpace = nameSpacePrefix.concat(".").concat(nameSpace);
                        ((ObjectNode) root).replace("namespace", JsonNodeFactory.instance.textNode(newNameSpace));
                    }

                }

                // continue traversing
                fieldNames.forEach(fieldName -> {
                    JsonNode fieldValue = root.get(fieldName);
                    correct(fieldValue);
                });

            } else if (root.isArray()) {
                ArrayNode arrayNode = (ArrayNode) root;

                for (int i = 0; i < arrayNode.size(); i++) {
                    JsonNode arrayElement = arrayNode.get(i);
                    correct(arrayElement);
                }
            } else {
                // JsonNode root represents a single value field - do something with it.
                // no need to do anything on fields so far
            }
        }
    }


    private Schema extractRelevantSubSchema(Schema rawSchema) {
        Schema.Field response = rawSchema.getField("json_" + FilenameUtils.removeExtension(wadlFile.getName()) + "Response");
        Schema responseSchema = response.schema().getTypes().get(1); // 1 to get the non-null type TODO: write safe
        Schema BusinessPartnerIDFieldSchema = responseSchema.getField("BusinessPartnerID").schema().getTypes().get(1);
        Schema jsonResponseRowSchema = BusinessPartnerIDFieldSchema.getField("Row").schema().getTypes().get(1);
        SchemaBuilder.RecordBuilder<Schema> builder = SchemaBuilder.record(responseSchema.getName()).namespace(FilenameUtils.removeExtension(wadlFile.getName()));
        return builder.fields().name("BusinessPartnerID").type(jsonResponseRowSchema).noDefault().endRecord();
    }


    private Schema convertXsdToAvro(String XsdSchema) throws IOException {
        // need to write out the schemata into temporary files to use
        // external lib for conversion
        Path tempDir = Files.createTempDirectory("converterTemp");
        Path tempXsdFile = Files.createTempFile(tempDir, "schema", "xsd");
        try (PrintWriter out = new PrintWriter(tempXsdFile.toFile())) {
            out.println(XsdSchema);
        }
        Path tempAvscFile = Files.createTempFile(tempDir, "schema", "avsc");
        // construct input arguments for converter tool
        ArrayList<String> input = new ArrayList<>();
        input.add("--config");
        input.add(Paths.get("src/main/resources/config.yml").toString());
        input.add("--baseDir");
        input.add(tempDir.toString());
        input.add("--toAvsc");
        input.add(tempXsdFile.getFileName().toString());
        input.add(tempAvscFile.getFileName().toString());
        in.dreamlabs.xmlavro.Converter.main(input.toArray(new String[0]));
        return new Schema.Parser().parse(tempAvscFile.toFile());
    }


    private String getResponseSchemaFromXmlExport(File wadlFile) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = factory.newDocumentBuilder();
        Document doc = dBuilder.parse(wadlFile);
        doc.getDocumentElement().normalize();
        Node grammars = getChildNodeFromElementByNodeName(doc.getDocumentElement(), "grammars");
        Node allSchemata = grammars.getChildNodes().item(3);
        return nodeToString(allSchemata);
    }

    private String nodeToString(Node node) {
        StringWriter sw = new StringWriter();
        try {
            Transformer t = TransformerFactory.newInstance().newTransformer();
            t.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            t.setOutputProperty(OutputKeys.INDENT, "yes");
            t.transform(new DOMSource(node), new StreamResult(sw));
        } catch (TransformerException te) {
            throw new RuntimeException("nodeToString Transformer Exception", te);
        }
        return sw.toString();
    }

    static Node getChildNodeFromElementByNodeName(Element node, String nodeName) {
        int index = -1;
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            if (children.item(i).getNodeName().equals(nodeName)) {
                index = i;
            }
        }
        if (index < 0) {
            throw new IllegalStateException("node name not present");
        }
        return children.item(index);
    }

}
