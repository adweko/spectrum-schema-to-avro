package com.helvetia.app.mdm.spectrum;

import org.apache.commons.cli.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

public class ConvertSpectrumWADLToAvroSchema {

    static Option wadl = Option.builder()
            .argName("wadl")
            .longOpt("wadl")
            .hasArg()
            .required()
            .type(String.class)
            .build();

    static Option output = Option.builder()
            .argName("avsc")
            .longOpt("avsc")
            .hasArg()
            .required()
            .type(String.class)
            .build();

    static Option nameSpacePrefix = Option.builder()
            .argName("p")
            .longOpt("prefix")
            .hasArg()
            .required()
            .type(String.class)
            .build();

    static Options options = new Options();


    public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
        CommandLineParser parser = new DefaultParser();
        options.addOption(wadl)
                .addOption(output)
                .addOption(nameSpacePrefix);
        try {
            // parse the command line arguments
            CommandLine line = parser.parse(options, args);
            File wadlFile = Paths.get(line.getOptionValue("wadl")).toFile();
            File avscFile = Paths.get(line.getOptionValue("avsc")).toFile();
            String prefix = line.getOptionValue("prefix");
            Converter converter = new Converter(wadlFile, avscFile, prefix);
            converter.convert();

        } catch (ParseException exp) {
            // oops, something went wrong
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
        }
    }
}
