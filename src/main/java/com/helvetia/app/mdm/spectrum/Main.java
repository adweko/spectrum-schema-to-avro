package com.helvetia.app.mdm.spectrum;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;
import org.apache.commons.io.FilenameUtils;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

// convert all schemata in src/main/resources/wadl
// to Avro and put them into src/main/resources/avro
// then, create the combined schema for the change topic
public class Main {

    private static void convert(Path wadl, Path avsc, String nameSpacePrefix) {
        try {
            ConvertSpectrumWADLToAvroSchema.main(new String[]{"--wadl", wadl.toAbsolutePath().toString(),
                    "--avsc", avsc.toAbsolutePath().toString(),
                    "--prefix", nameSpacePrefix
            });
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws IOException {
        Path wadl;
        wadl = Paths.get("src/main/resources/schemata/wadl/readFullPartnerAllVersionsFromDate.xml");
        System.out.println("converting " + wadl.toAbsolutePath());
        convert(wadl, Paths.get("src/main/resources/schemata/avro/"
                        + FilenameUtils.removeExtension(wadl.getFileName().toString())
                        + ".avsc"),
                "statePartner");

        wadl = Paths.get("src/main/resources/schemata/wadl/inboundTrafoZepasChannel.xml");
        System.out.println("converting " + wadl.toAbsolutePath());
        convert(wadl, Paths.get("src/main/resources/schemata/avro/"
                + FilenameUtils.removeExtension(wadl.getFileName().toString())
                + ".avsc"),
                "inboundTrafoZepasChannel");

        wadl = Paths.get("src/main/resources/schemata/wadl/inboundTrafoZepasLegalPerson.xml");
        System.out.println("converting " + wadl.toAbsolutePath());
        convert(wadl, Paths.get("src/main/resources/schemata/avro/"
                        + FilenameUtils.removeExtension(wadl.getFileName().toString())
                        + ".avsc"),
                "inboundTrafoZepasLegalPerson");

        wadl = Paths.get("src/main/resources/schemata/wadl/inboundTrafoZepasLocation.xml");
        System.out.println("converting " + wadl.toAbsolutePath());
        convert(wadl, Paths.get("src/main/resources/schemata/avro/"
                        + FilenameUtils.removeExtension(wadl.getFileName().toString())
                        + ".avsc"),
                "inboundTrafoZepasLocation");

        wadl = Paths.get("src/main/resources/schemata/wadl/inboundTrafoZepasNaturalPerson.xml");
        System.out.println("converting " + wadl.toAbsolutePath());
        convert(wadl, Paths.get("src/main/resources/schemata/avro/"
                        + FilenameUtils.removeExtension(wadl.getFileName().toString())
                        + ".avsc"),
                "inboundTrafoZepasNaturalPerson");

        wadl = Paths.get("src/main/resources/schemata/wadl/inboundTrafoZepasUnderwriting.xml");
        System.out.println("converting " + wadl.toAbsolutePath());
        convert(wadl, Paths.get("src/main/resources/schemata/avro/"
                        + FilenameUtils.removeExtension(wadl.getFileName().toString())
                        + ".avsc"),
                "inboundTrafoZepasUnderwriting");

        wadl = Paths.get("src/main/resources/schemata/wadl/inboundTrafoZepasDuplicates.xml");
        System.out.println("converting " + wadl.toAbsolutePath());
        convert(wadl, Paths.get("src/main/resources/schemata/avro/"
                        + FilenameUtils.removeExtension(wadl.getFileName().toString())
                        + ".avsc"),
                "inboundTrafoZepasDuplicates");



        System.out.println("Now creating the mdm change topic schema. All filenames and paths are hard-coded here, so " +
                "change the code if these ever change.");
        Schema changeTopicSchema = createChangeTopicSchema();

        try (PrintWriter out = new PrintWriter("src/main/resources/schemata/avro/changes.avsc")) {
            out.write(changeTopicSchema.toString(true));
        }

    }

    // combined schema has to correspond to the logic applied within the CVBoost inbound workflows
    // see processTransaction.groovy in those
    private static Schema createChangeTopicSchema() throws IOException {
        // channel represents the sub-schema for both channel and location, corresponding to hasChannelId in
        // the data model
        final Schema fullSchemaChannel = new Schema.Parser().parse(
                Paths.get("src/main/resources/schemata/avro/inboundTrafoZepasChannel.avsc").toFile());
        final Schema fullSchemaLegalPerson = new Schema.Parser().parse(
                Paths.get("src/main/resources/schemata/avro/inboundTrafoZepasLegalPerson.avsc").toFile());
        final Schema fullSchemaNaturalPerson = new Schema.Parser().parse(
                Paths.get("src/main/resources/schemata/avro/inboundTrafoZepasNaturalPerson.avsc").toFile());
        final Schema fullSchemaUnderwriting = new Schema.Parser().parse(
                Paths.get("src/main/resources/schemata/avro/inboundTrafoZepasUnderwriting.avsc").toFile());
        final Schema fullSchemaDuplicates = new Schema.Parser().parse(
                Paths.get("src/main/resources/schemata/avro/inboundTrafoZepasDuplicates.avsc").toFile());

        // entity-specific schemata
        final Schema schemaChannel = fullSchemaChannel.getField("BusinessPartnerID").schema()
                .getElementType().getField("hasChannelID").schema();
        final Schema schemaLegalPerson = fullSchemaLegalPerson.getField("BusinessPartnerID").schema()
                .getElementType().getField("isLegalPerson").schema();
        final Schema schemaNaturalPerson = fullSchemaNaturalPerson.getField("BusinessPartnerID").schema()
                .getElementType().getField("isNaturalPerson").schema();
        final Schema schemaUnderwriting = fullSchemaUnderwriting.getField("BusinessPartnerID").schema()
                .getElementType().getField("hasUnderwritingCodeID").schema();
        final Schema schemaDuplicates = fullSchemaDuplicates.getField("BusinessPartnerID").schema()
                .getElementType().getField("isDuplicate").schema();

        SchemaBuilder.FieldAssembler<Schema> schemaFieldAssembler = SchemaBuilder.record("Row").namespace("changePartner.json.Response").fields()
                .optionalString("TECHInternalPID_Location")
                .optionalString("TECHInternalPID_Channel")
                .optionalString("TECHInternalPID_NaturalPerson")
                .optionalString("TECHInternalPID_LegalPerson")
                .optionalString("TECHInternalPID_Underwriting")
                .optionalString("TECHInternalPID_Duplicates")
                .optionalString("TECHInternalPID")
                .name("hasChannelID").type(schemaChannel).withDefault(null)
                .name("isLegalPerson").type(schemaLegalPerson).withDefault(null)
                .name("isNaturalPerson").type(schemaNaturalPerson).withDefault(null)
                .name("hasUnderwritingCodeID").type(schemaUnderwriting).withDefault(null)
                .name("isDuplicate").type(schemaDuplicates).withDefault(null);

        // default fields of a response row
        Schema responseRowSchemaLegalPerson = fullSchemaLegalPerson.getField("BusinessPartnerID").schema()
                .getElementType();

        Map<String, Schema> defaultFieldsMap = responseRowSchemaLegalPerson.getFields().stream()
                .filter(f -> (!f.name().equals("hasChannelID")) &&
                        (!f.name().equals("isLegalPerson")) &&
                        (!f.name().equals("isNaturalPerson")) &&
                        (!f.name().equals("hasUnderwritingCodeID")) &&
                        (!f.name().equals("isDuplicate")) &&
                        (!f.name().equals("TECHInternalPID"))
                ).collect(
                        Collectors.toMap(Schema.Field::name, Schema.Field::schema)
                );
        defaultFieldsMap.forEach((name, schema) -> schemaFieldAssembler.name(name).type(schema).withDefault(null));

        Schema changeResponseRowSchema = schemaFieldAssembler.endRecord();

        final Schema schema = SchemaBuilder.record("changeMdmResponse").namespace("changePartner.com.helvetia.custom").fields()
                .name("BusinessPartnerID").type().array().items(
                        changeResponseRowSchema
                )
                .arrayDefault(Collections.emptyList())
                .endRecord();
        return schema;
    }

}
