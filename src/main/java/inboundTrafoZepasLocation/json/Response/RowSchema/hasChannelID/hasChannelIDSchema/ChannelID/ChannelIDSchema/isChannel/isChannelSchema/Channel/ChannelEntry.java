/**
 * Autogenerated by Avro
 *
 * DO NOT EDIT DIRECTLY
 */
package inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel;

import org.apache.avro.specific.SpecificData;
import org.apache.avro.message.BinaryMessageEncoder;
import org.apache.avro.message.BinaryMessageDecoder;
import org.apache.avro.message.SchemaStore;

@SuppressWarnings("all")
@org.apache.avro.specific.AvroGenerated
public class ChannelEntry extends org.apache.avro.specific.SpecificRecordBase implements org.apache.avro.specific.SpecificRecord {
  private static final long serialVersionUID = 7875260754408831761L;
  public static final org.apache.avro.Schema SCHEMA$ = new org.apache.avro.Schema.Parser().parse("{\"type\":\"record\",\"name\":\"ChannelEntry\",\"namespace\":\"inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel\",\"fields\":[{\"name\":\"channelNote\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:channelNote\"},{\"name\":\"noAds\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:noAds\"},{\"name\":\"preferredLanguage\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:preferredLanguage\"},{\"name\":\"privateBusiness\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:privateBusiness\"},{\"name\":\"routing\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:routing\"},{\"name\":\"usage\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:usage\"},{\"name\":\"TECHExternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:TECHExternalPID\"},{\"name\":\"TECHInternalPID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:TECHInternalPID\"},{\"name\":\"TECHMDMPartnerID\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:TECHMDMPartnerID\"},{\"name\":\"TECHSourceSystem\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:TECHSourceSystem\"},{\"name\":\"TECHUser\",\"type\":[\"null\",\"string\"],\"default\":null,\"source\":\"element http://www.pb.com/spectrum/services/inboundTrafoZepasLocation:TECHUser\"}]}");
  public static org.apache.avro.Schema getClassSchema() { return SCHEMA$; }

  private static SpecificData MODEL$ = new SpecificData();

  private static final BinaryMessageEncoder<ChannelEntry> ENCODER =
      new BinaryMessageEncoder<ChannelEntry>(MODEL$, SCHEMA$);

  private static final BinaryMessageDecoder<ChannelEntry> DECODER =
      new BinaryMessageDecoder<ChannelEntry>(MODEL$, SCHEMA$);

  /**
   * Return the BinaryMessageDecoder instance used by this class.
   */
  public static BinaryMessageDecoder<ChannelEntry> getDecoder() {
    return DECODER;
  }

  /**
   * Create a new BinaryMessageDecoder instance for this class that uses the specified {@link SchemaStore}.
   * @param resolver a {@link SchemaStore} used to find schemas by fingerprint
   */
  public static BinaryMessageDecoder<ChannelEntry> createDecoder(SchemaStore resolver) {
    return new BinaryMessageDecoder<ChannelEntry>(MODEL$, SCHEMA$, resolver);
  }

  /** Serializes this ChannelEntry to a ByteBuffer. */
  public java.nio.ByteBuffer toByteBuffer() throws java.io.IOException {
    return ENCODER.encode(this);
  }

  /** Deserializes a ChannelEntry from a ByteBuffer. */
  public static ChannelEntry fromByteBuffer(
      java.nio.ByteBuffer b) throws java.io.IOException {
    return DECODER.decode(b);
  }

  @Deprecated public java.lang.CharSequence channelNote;
  @Deprecated public java.lang.CharSequence noAds;
  @Deprecated public java.lang.CharSequence preferredLanguage;
  @Deprecated public java.lang.CharSequence privateBusiness;
  @Deprecated public java.lang.CharSequence routing;
  @Deprecated public java.lang.CharSequence usage;
  @Deprecated public java.lang.CharSequence TECHExternalPID;
  @Deprecated public java.lang.CharSequence TECHInternalPID;
  @Deprecated public java.lang.CharSequence TECHMDMPartnerID;
  @Deprecated public java.lang.CharSequence TECHSourceSystem;
  @Deprecated public java.lang.CharSequence TECHUser;

  /**
   * Default constructor.  Note that this does not initialize fields
   * to their default values from the schema.  If that is desired then
   * one should use <code>newBuilder()</code>.
   */
  public ChannelEntry() {}

  /**
   * All-args constructor.
   * @param channelNote The new value for channelNote
   * @param noAds The new value for noAds
   * @param preferredLanguage The new value for preferredLanguage
   * @param privateBusiness The new value for privateBusiness
   * @param routing The new value for routing
   * @param usage The new value for usage
   * @param TECHExternalPID The new value for TECHExternalPID
   * @param TECHInternalPID The new value for TECHInternalPID
   * @param TECHMDMPartnerID The new value for TECHMDMPartnerID
   * @param TECHSourceSystem The new value for TECHSourceSystem
   * @param TECHUser The new value for TECHUser
   */
  public ChannelEntry(java.lang.CharSequence channelNote, java.lang.CharSequence noAds, java.lang.CharSequence preferredLanguage, java.lang.CharSequence privateBusiness, java.lang.CharSequence routing, java.lang.CharSequence usage, java.lang.CharSequence TECHExternalPID, java.lang.CharSequence TECHInternalPID, java.lang.CharSequence TECHMDMPartnerID, java.lang.CharSequence TECHSourceSystem, java.lang.CharSequence TECHUser) {
    this.channelNote = channelNote;
    this.noAds = noAds;
    this.preferredLanguage = preferredLanguage;
    this.privateBusiness = privateBusiness;
    this.routing = routing;
    this.usage = usage;
    this.TECHExternalPID = TECHExternalPID;
    this.TECHInternalPID = TECHInternalPID;
    this.TECHMDMPartnerID = TECHMDMPartnerID;
    this.TECHSourceSystem = TECHSourceSystem;
    this.TECHUser = TECHUser;
  }

  public org.apache.avro.Schema getSchema() { return SCHEMA$; }
  // Used by DatumWriter.  Applications should not call.
  public java.lang.Object get(int field$) {
    switch (field$) {
    case 0: return channelNote;
    case 1: return noAds;
    case 2: return preferredLanguage;
    case 3: return privateBusiness;
    case 4: return routing;
    case 5: return usage;
    case 6: return TECHExternalPID;
    case 7: return TECHInternalPID;
    case 8: return TECHMDMPartnerID;
    case 9: return TECHSourceSystem;
    case 10: return TECHUser;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  // Used by DatumReader.  Applications should not call.
  @SuppressWarnings(value="unchecked")
  public void put(int field$, java.lang.Object value$) {
    switch (field$) {
    case 0: channelNote = (java.lang.CharSequence)value$; break;
    case 1: noAds = (java.lang.CharSequence)value$; break;
    case 2: preferredLanguage = (java.lang.CharSequence)value$; break;
    case 3: privateBusiness = (java.lang.CharSequence)value$; break;
    case 4: routing = (java.lang.CharSequence)value$; break;
    case 5: usage = (java.lang.CharSequence)value$; break;
    case 6: TECHExternalPID = (java.lang.CharSequence)value$; break;
    case 7: TECHInternalPID = (java.lang.CharSequence)value$; break;
    case 8: TECHMDMPartnerID = (java.lang.CharSequence)value$; break;
    case 9: TECHSourceSystem = (java.lang.CharSequence)value$; break;
    case 10: TECHUser = (java.lang.CharSequence)value$; break;
    default: throw new org.apache.avro.AvroRuntimeException("Bad index");
    }
  }

  /**
   * Gets the value of the 'channelNote' field.
   * @return The value of the 'channelNote' field.
   */
  public java.lang.CharSequence getChannelNote() {
    return channelNote;
  }

  /**
   * Sets the value of the 'channelNote' field.
   * @param value the value to set.
   */
  public void setChannelNote(java.lang.CharSequence value) {
    this.channelNote = value;
  }

  /**
   * Gets the value of the 'noAds' field.
   * @return The value of the 'noAds' field.
   */
  public java.lang.CharSequence getNoAds() {
    return noAds;
  }

  /**
   * Sets the value of the 'noAds' field.
   * @param value the value to set.
   */
  public void setNoAds(java.lang.CharSequence value) {
    this.noAds = value;
  }

  /**
   * Gets the value of the 'preferredLanguage' field.
   * @return The value of the 'preferredLanguage' field.
   */
  public java.lang.CharSequence getPreferredLanguage() {
    return preferredLanguage;
  }

  /**
   * Sets the value of the 'preferredLanguage' field.
   * @param value the value to set.
   */
  public void setPreferredLanguage(java.lang.CharSequence value) {
    this.preferredLanguage = value;
  }

  /**
   * Gets the value of the 'privateBusiness' field.
   * @return The value of the 'privateBusiness' field.
   */
  public java.lang.CharSequence getPrivateBusiness() {
    return privateBusiness;
  }

  /**
   * Sets the value of the 'privateBusiness' field.
   * @param value the value to set.
   */
  public void setPrivateBusiness(java.lang.CharSequence value) {
    this.privateBusiness = value;
  }

  /**
   * Gets the value of the 'routing' field.
   * @return The value of the 'routing' field.
   */
  public java.lang.CharSequence getRouting() {
    return routing;
  }

  /**
   * Sets the value of the 'routing' field.
   * @param value the value to set.
   */
  public void setRouting(java.lang.CharSequence value) {
    this.routing = value;
  }

  /**
   * Gets the value of the 'usage' field.
   * @return The value of the 'usage' field.
   */
  public java.lang.CharSequence getUsage() {
    return usage;
  }

  /**
   * Sets the value of the 'usage' field.
   * @param value the value to set.
   */
  public void setUsage(java.lang.CharSequence value) {
    this.usage = value;
  }

  /**
   * Gets the value of the 'TECHExternalPID' field.
   * @return The value of the 'TECHExternalPID' field.
   */
  public java.lang.CharSequence getTECHExternalPID() {
    return TECHExternalPID;
  }

  /**
   * Sets the value of the 'TECHExternalPID' field.
   * @param value the value to set.
   */
  public void setTECHExternalPID(java.lang.CharSequence value) {
    this.TECHExternalPID = value;
  }

  /**
   * Gets the value of the 'TECHInternalPID' field.
   * @return The value of the 'TECHInternalPID' field.
   */
  public java.lang.CharSequence getTECHInternalPID() {
    return TECHInternalPID;
  }

  /**
   * Sets the value of the 'TECHInternalPID' field.
   * @param value the value to set.
   */
  public void setTECHInternalPID(java.lang.CharSequence value) {
    this.TECHInternalPID = value;
  }

  /**
   * Gets the value of the 'TECHMDMPartnerID' field.
   * @return The value of the 'TECHMDMPartnerID' field.
   */
  public java.lang.CharSequence getTECHMDMPartnerID() {
    return TECHMDMPartnerID;
  }

  /**
   * Sets the value of the 'TECHMDMPartnerID' field.
   * @param value the value to set.
   */
  public void setTECHMDMPartnerID(java.lang.CharSequence value) {
    this.TECHMDMPartnerID = value;
  }

  /**
   * Gets the value of the 'TECHSourceSystem' field.
   * @return The value of the 'TECHSourceSystem' field.
   */
  public java.lang.CharSequence getTECHSourceSystem() {
    return TECHSourceSystem;
  }

  /**
   * Sets the value of the 'TECHSourceSystem' field.
   * @param value the value to set.
   */
  public void setTECHSourceSystem(java.lang.CharSequence value) {
    this.TECHSourceSystem = value;
  }

  /**
   * Gets the value of the 'TECHUser' field.
   * @return The value of the 'TECHUser' field.
   */
  public java.lang.CharSequence getTECHUser() {
    return TECHUser;
  }

  /**
   * Sets the value of the 'TECHUser' field.
   * @param value the value to set.
   */
  public void setTECHUser(java.lang.CharSequence value) {
    this.TECHUser = value;
  }

  /**
   * Creates a new ChannelEntry RecordBuilder.
   * @return A new ChannelEntry RecordBuilder
   */
  public static inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder newBuilder() {
    return new inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder();
  }

  /**
   * Creates a new ChannelEntry RecordBuilder by copying an existing Builder.
   * @param other The existing builder to copy.
   * @return A new ChannelEntry RecordBuilder
   */
  public static inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder newBuilder(inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder other) {
    return new inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder(other);
  }

  /**
   * Creates a new ChannelEntry RecordBuilder by copying an existing ChannelEntry instance.
   * @param other The existing instance to copy.
   * @return A new ChannelEntry RecordBuilder
   */
  public static inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder newBuilder(inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry other) {
    return new inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder(other);
  }

  /**
   * RecordBuilder for ChannelEntry instances.
   */
  public static class Builder extends org.apache.avro.specific.SpecificRecordBuilderBase<ChannelEntry>
    implements org.apache.avro.data.RecordBuilder<ChannelEntry> {

    private java.lang.CharSequence channelNote;
    private java.lang.CharSequence noAds;
    private java.lang.CharSequence preferredLanguage;
    private java.lang.CharSequence privateBusiness;
    private java.lang.CharSequence routing;
    private java.lang.CharSequence usage;
    private java.lang.CharSequence TECHExternalPID;
    private java.lang.CharSequence TECHInternalPID;
    private java.lang.CharSequence TECHMDMPartnerID;
    private java.lang.CharSequence TECHSourceSystem;
    private java.lang.CharSequence TECHUser;

    /** Creates a new Builder */
    private Builder() {
      super(SCHEMA$);
    }

    /**
     * Creates a Builder by copying an existing Builder.
     * @param other The existing Builder to copy.
     */
    private Builder(inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder other) {
      super(other);
      if (isValidValue(fields()[0], other.channelNote)) {
        this.channelNote = data().deepCopy(fields()[0].schema(), other.channelNote);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.noAds)) {
        this.noAds = data().deepCopy(fields()[1].schema(), other.noAds);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.preferredLanguage)) {
        this.preferredLanguage = data().deepCopy(fields()[2].schema(), other.preferredLanguage);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.privateBusiness)) {
        this.privateBusiness = data().deepCopy(fields()[3].schema(), other.privateBusiness);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.routing)) {
        this.routing = data().deepCopy(fields()[4].schema(), other.routing);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.usage)) {
        this.usage = data().deepCopy(fields()[5].schema(), other.usage);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.TECHExternalPID)) {
        this.TECHExternalPID = data().deepCopy(fields()[6].schema(), other.TECHExternalPID);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.TECHInternalPID)) {
        this.TECHInternalPID = data().deepCopy(fields()[7].schema(), other.TECHInternalPID);
        fieldSetFlags()[7] = true;
      }
      if (isValidValue(fields()[8], other.TECHMDMPartnerID)) {
        this.TECHMDMPartnerID = data().deepCopy(fields()[8].schema(), other.TECHMDMPartnerID);
        fieldSetFlags()[8] = true;
      }
      if (isValidValue(fields()[9], other.TECHSourceSystem)) {
        this.TECHSourceSystem = data().deepCopy(fields()[9].schema(), other.TECHSourceSystem);
        fieldSetFlags()[9] = true;
      }
      if (isValidValue(fields()[10], other.TECHUser)) {
        this.TECHUser = data().deepCopy(fields()[10].schema(), other.TECHUser);
        fieldSetFlags()[10] = true;
      }
    }

    /**
     * Creates a Builder by copying an existing ChannelEntry instance
     * @param other The existing instance to copy.
     */
    private Builder(inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry other) {
            super(SCHEMA$);
      if (isValidValue(fields()[0], other.channelNote)) {
        this.channelNote = data().deepCopy(fields()[0].schema(), other.channelNote);
        fieldSetFlags()[0] = true;
      }
      if (isValidValue(fields()[1], other.noAds)) {
        this.noAds = data().deepCopy(fields()[1].schema(), other.noAds);
        fieldSetFlags()[1] = true;
      }
      if (isValidValue(fields()[2], other.preferredLanguage)) {
        this.preferredLanguage = data().deepCopy(fields()[2].schema(), other.preferredLanguage);
        fieldSetFlags()[2] = true;
      }
      if (isValidValue(fields()[3], other.privateBusiness)) {
        this.privateBusiness = data().deepCopy(fields()[3].schema(), other.privateBusiness);
        fieldSetFlags()[3] = true;
      }
      if (isValidValue(fields()[4], other.routing)) {
        this.routing = data().deepCopy(fields()[4].schema(), other.routing);
        fieldSetFlags()[4] = true;
      }
      if (isValidValue(fields()[5], other.usage)) {
        this.usage = data().deepCopy(fields()[5].schema(), other.usage);
        fieldSetFlags()[5] = true;
      }
      if (isValidValue(fields()[6], other.TECHExternalPID)) {
        this.TECHExternalPID = data().deepCopy(fields()[6].schema(), other.TECHExternalPID);
        fieldSetFlags()[6] = true;
      }
      if (isValidValue(fields()[7], other.TECHInternalPID)) {
        this.TECHInternalPID = data().deepCopy(fields()[7].schema(), other.TECHInternalPID);
        fieldSetFlags()[7] = true;
      }
      if (isValidValue(fields()[8], other.TECHMDMPartnerID)) {
        this.TECHMDMPartnerID = data().deepCopy(fields()[8].schema(), other.TECHMDMPartnerID);
        fieldSetFlags()[8] = true;
      }
      if (isValidValue(fields()[9], other.TECHSourceSystem)) {
        this.TECHSourceSystem = data().deepCopy(fields()[9].schema(), other.TECHSourceSystem);
        fieldSetFlags()[9] = true;
      }
      if (isValidValue(fields()[10], other.TECHUser)) {
        this.TECHUser = data().deepCopy(fields()[10].schema(), other.TECHUser);
        fieldSetFlags()[10] = true;
      }
    }

    /**
      * Gets the value of the 'channelNote' field.
      * @return The value.
      */
    public java.lang.CharSequence getChannelNote() {
      return channelNote;
    }

    /**
      * Sets the value of the 'channelNote' field.
      * @param value The value of 'channelNote'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setChannelNote(java.lang.CharSequence value) {
      validate(fields()[0], value);
      this.channelNote = value;
      fieldSetFlags()[0] = true;
      return this;
    }

    /**
      * Checks whether the 'channelNote' field has been set.
      * @return True if the 'channelNote' field has been set, false otherwise.
      */
    public boolean hasChannelNote() {
      return fieldSetFlags()[0];
    }


    /**
      * Clears the value of the 'channelNote' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearChannelNote() {
      channelNote = null;
      fieldSetFlags()[0] = false;
      return this;
    }

    /**
      * Gets the value of the 'noAds' field.
      * @return The value.
      */
    public java.lang.CharSequence getNoAds() {
      return noAds;
    }

    /**
      * Sets the value of the 'noAds' field.
      * @param value The value of 'noAds'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setNoAds(java.lang.CharSequence value) {
      validate(fields()[1], value);
      this.noAds = value;
      fieldSetFlags()[1] = true;
      return this;
    }

    /**
      * Checks whether the 'noAds' field has been set.
      * @return True if the 'noAds' field has been set, false otherwise.
      */
    public boolean hasNoAds() {
      return fieldSetFlags()[1];
    }


    /**
      * Clears the value of the 'noAds' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearNoAds() {
      noAds = null;
      fieldSetFlags()[1] = false;
      return this;
    }

    /**
      * Gets the value of the 'preferredLanguage' field.
      * @return The value.
      */
    public java.lang.CharSequence getPreferredLanguage() {
      return preferredLanguage;
    }

    /**
      * Sets the value of the 'preferredLanguage' field.
      * @param value The value of 'preferredLanguage'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setPreferredLanguage(java.lang.CharSequence value) {
      validate(fields()[2], value);
      this.preferredLanguage = value;
      fieldSetFlags()[2] = true;
      return this;
    }

    /**
      * Checks whether the 'preferredLanguage' field has been set.
      * @return True if the 'preferredLanguage' field has been set, false otherwise.
      */
    public boolean hasPreferredLanguage() {
      return fieldSetFlags()[2];
    }


    /**
      * Clears the value of the 'preferredLanguage' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearPreferredLanguage() {
      preferredLanguage = null;
      fieldSetFlags()[2] = false;
      return this;
    }

    /**
      * Gets the value of the 'privateBusiness' field.
      * @return The value.
      */
    public java.lang.CharSequence getPrivateBusiness() {
      return privateBusiness;
    }

    /**
      * Sets the value of the 'privateBusiness' field.
      * @param value The value of 'privateBusiness'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setPrivateBusiness(java.lang.CharSequence value) {
      validate(fields()[3], value);
      this.privateBusiness = value;
      fieldSetFlags()[3] = true;
      return this;
    }

    /**
      * Checks whether the 'privateBusiness' field has been set.
      * @return True if the 'privateBusiness' field has been set, false otherwise.
      */
    public boolean hasPrivateBusiness() {
      return fieldSetFlags()[3];
    }


    /**
      * Clears the value of the 'privateBusiness' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearPrivateBusiness() {
      privateBusiness = null;
      fieldSetFlags()[3] = false;
      return this;
    }

    /**
      * Gets the value of the 'routing' field.
      * @return The value.
      */
    public java.lang.CharSequence getRouting() {
      return routing;
    }

    /**
      * Sets the value of the 'routing' field.
      * @param value The value of 'routing'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setRouting(java.lang.CharSequence value) {
      validate(fields()[4], value);
      this.routing = value;
      fieldSetFlags()[4] = true;
      return this;
    }

    /**
      * Checks whether the 'routing' field has been set.
      * @return True if the 'routing' field has been set, false otherwise.
      */
    public boolean hasRouting() {
      return fieldSetFlags()[4];
    }


    /**
      * Clears the value of the 'routing' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearRouting() {
      routing = null;
      fieldSetFlags()[4] = false;
      return this;
    }

    /**
      * Gets the value of the 'usage' field.
      * @return The value.
      */
    public java.lang.CharSequence getUsage() {
      return usage;
    }

    /**
      * Sets the value of the 'usage' field.
      * @param value The value of 'usage'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setUsage(java.lang.CharSequence value) {
      validate(fields()[5], value);
      this.usage = value;
      fieldSetFlags()[5] = true;
      return this;
    }

    /**
      * Checks whether the 'usage' field has been set.
      * @return True if the 'usage' field has been set, false otherwise.
      */
    public boolean hasUsage() {
      return fieldSetFlags()[5];
    }


    /**
      * Clears the value of the 'usage' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearUsage() {
      usage = null;
      fieldSetFlags()[5] = false;
      return this;
    }

    /**
      * Gets the value of the 'TECHExternalPID' field.
      * @return The value.
      */
    public java.lang.CharSequence getTECHExternalPID() {
      return TECHExternalPID;
    }

    /**
      * Sets the value of the 'TECHExternalPID' field.
      * @param value The value of 'TECHExternalPID'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setTECHExternalPID(java.lang.CharSequence value) {
      validate(fields()[6], value);
      this.TECHExternalPID = value;
      fieldSetFlags()[6] = true;
      return this;
    }

    /**
      * Checks whether the 'TECHExternalPID' field has been set.
      * @return True if the 'TECHExternalPID' field has been set, false otherwise.
      */
    public boolean hasTECHExternalPID() {
      return fieldSetFlags()[6];
    }


    /**
      * Clears the value of the 'TECHExternalPID' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearTECHExternalPID() {
      TECHExternalPID = null;
      fieldSetFlags()[6] = false;
      return this;
    }

    /**
      * Gets the value of the 'TECHInternalPID' field.
      * @return The value.
      */
    public java.lang.CharSequence getTECHInternalPID() {
      return TECHInternalPID;
    }

    /**
      * Sets the value of the 'TECHInternalPID' field.
      * @param value The value of 'TECHInternalPID'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setTECHInternalPID(java.lang.CharSequence value) {
      validate(fields()[7], value);
      this.TECHInternalPID = value;
      fieldSetFlags()[7] = true;
      return this;
    }

    /**
      * Checks whether the 'TECHInternalPID' field has been set.
      * @return True if the 'TECHInternalPID' field has been set, false otherwise.
      */
    public boolean hasTECHInternalPID() {
      return fieldSetFlags()[7];
    }


    /**
      * Clears the value of the 'TECHInternalPID' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearTECHInternalPID() {
      TECHInternalPID = null;
      fieldSetFlags()[7] = false;
      return this;
    }

    /**
      * Gets the value of the 'TECHMDMPartnerID' field.
      * @return The value.
      */
    public java.lang.CharSequence getTECHMDMPartnerID() {
      return TECHMDMPartnerID;
    }

    /**
      * Sets the value of the 'TECHMDMPartnerID' field.
      * @param value The value of 'TECHMDMPartnerID'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setTECHMDMPartnerID(java.lang.CharSequence value) {
      validate(fields()[8], value);
      this.TECHMDMPartnerID = value;
      fieldSetFlags()[8] = true;
      return this;
    }

    /**
      * Checks whether the 'TECHMDMPartnerID' field has been set.
      * @return True if the 'TECHMDMPartnerID' field has been set, false otherwise.
      */
    public boolean hasTECHMDMPartnerID() {
      return fieldSetFlags()[8];
    }


    /**
      * Clears the value of the 'TECHMDMPartnerID' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearTECHMDMPartnerID() {
      TECHMDMPartnerID = null;
      fieldSetFlags()[8] = false;
      return this;
    }

    /**
      * Gets the value of the 'TECHSourceSystem' field.
      * @return The value.
      */
    public java.lang.CharSequence getTECHSourceSystem() {
      return TECHSourceSystem;
    }

    /**
      * Sets the value of the 'TECHSourceSystem' field.
      * @param value The value of 'TECHSourceSystem'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setTECHSourceSystem(java.lang.CharSequence value) {
      validate(fields()[9], value);
      this.TECHSourceSystem = value;
      fieldSetFlags()[9] = true;
      return this;
    }

    /**
      * Checks whether the 'TECHSourceSystem' field has been set.
      * @return True if the 'TECHSourceSystem' field has been set, false otherwise.
      */
    public boolean hasTECHSourceSystem() {
      return fieldSetFlags()[9];
    }


    /**
      * Clears the value of the 'TECHSourceSystem' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearTECHSourceSystem() {
      TECHSourceSystem = null;
      fieldSetFlags()[9] = false;
      return this;
    }

    /**
      * Gets the value of the 'TECHUser' field.
      * @return The value.
      */
    public java.lang.CharSequence getTECHUser() {
      return TECHUser;
    }

    /**
      * Sets the value of the 'TECHUser' field.
      * @param value The value of 'TECHUser'.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder setTECHUser(java.lang.CharSequence value) {
      validate(fields()[10], value);
      this.TECHUser = value;
      fieldSetFlags()[10] = true;
      return this;
    }

    /**
      * Checks whether the 'TECHUser' field has been set.
      * @return True if the 'TECHUser' field has been set, false otherwise.
      */
    public boolean hasTECHUser() {
      return fieldSetFlags()[10];
    }


    /**
      * Clears the value of the 'TECHUser' field.
      * @return This builder.
      */
    public inboundTrafoZepasLocation.json.Response.RowSchema.hasChannelID.hasChannelIDSchema.ChannelID.ChannelIDSchema.isChannel.isChannelSchema.Channel.ChannelEntry.Builder clearTECHUser() {
      TECHUser = null;
      fieldSetFlags()[10] = false;
      return this;
    }

    @Override
    @SuppressWarnings("unchecked")
    public ChannelEntry build() {
      try {
        ChannelEntry record = new ChannelEntry();
        record.channelNote = fieldSetFlags()[0] ? this.channelNote : (java.lang.CharSequence) defaultValue(fields()[0]);
        record.noAds = fieldSetFlags()[1] ? this.noAds : (java.lang.CharSequence) defaultValue(fields()[1]);
        record.preferredLanguage = fieldSetFlags()[2] ? this.preferredLanguage : (java.lang.CharSequence) defaultValue(fields()[2]);
        record.privateBusiness = fieldSetFlags()[3] ? this.privateBusiness : (java.lang.CharSequence) defaultValue(fields()[3]);
        record.routing = fieldSetFlags()[4] ? this.routing : (java.lang.CharSequence) defaultValue(fields()[4]);
        record.usage = fieldSetFlags()[5] ? this.usage : (java.lang.CharSequence) defaultValue(fields()[5]);
        record.TECHExternalPID = fieldSetFlags()[6] ? this.TECHExternalPID : (java.lang.CharSequence) defaultValue(fields()[6]);
        record.TECHInternalPID = fieldSetFlags()[7] ? this.TECHInternalPID : (java.lang.CharSequence) defaultValue(fields()[7]);
        record.TECHMDMPartnerID = fieldSetFlags()[8] ? this.TECHMDMPartnerID : (java.lang.CharSequence) defaultValue(fields()[8]);
        record.TECHSourceSystem = fieldSetFlags()[9] ? this.TECHSourceSystem : (java.lang.CharSequence) defaultValue(fields()[9]);
        record.TECHUser = fieldSetFlags()[10] ? this.TECHUser : (java.lang.CharSequence) defaultValue(fields()[10]);
        return record;
      } catch (java.lang.Exception e) {
        throw new org.apache.avro.AvroRuntimeException(e);
      }
    }
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumWriter<ChannelEntry>
    WRITER$ = (org.apache.avro.io.DatumWriter<ChannelEntry>)MODEL$.createDatumWriter(SCHEMA$);

  @Override public void writeExternal(java.io.ObjectOutput out)
    throws java.io.IOException {
    WRITER$.write(this, SpecificData.getEncoder(out));
  }

  @SuppressWarnings("unchecked")
  private static final org.apache.avro.io.DatumReader<ChannelEntry>
    READER$ = (org.apache.avro.io.DatumReader<ChannelEntry>)MODEL$.createDatumReader(SCHEMA$);

  @Override public void readExternal(java.io.ObjectInput in)
    throws java.io.IOException {
    READER$.read(this, SpecificData.getDecoder(in));
  }

}
